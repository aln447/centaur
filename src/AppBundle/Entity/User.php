<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(type="string", length=255, name="user_name")
     * @var string $userName
     */
    protected $userName;
    /**
     * @ORM\Column(type="string", length=255, name="first_name")
     * @var string $firstName
     */
    protected $firstName;
    /**
     * @ORM\Column(type="string", length=255, name="last_name")
     * @var string $lastName
     */
    protected $lastName;
    /**
     * @ORM\Column(type="integer", name="role")
     * @var integer $role
     */
    protected $role;
    /**
     * @ORM\Column(type="datetime", name="date_added")
     */
    protected $dateAdded;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}
